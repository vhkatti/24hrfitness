package importusers;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

/**
 * Class file that Imports User record from CSV into Okta.
 *
 * @author venkatesh.katti
 */
public class Import24HourFitnessPSCSVUsers {

	final static Logger logger = Logger.getLogger(Import24HourFitnessPSCSVUsers.class);

	private static String csvPath;
	private static String csvFile;
	private static String tenantUrl;
	private static String token;
	private static String archivePath;
	private static final String OKTA_LOCKED_GROUP_NAME = "Lock";

	public static String get(String resource, String token) {
		boolean tryAgain = true;
		String result = "";
		URL url;
		HttpURLConnection conn;
		BufferedReader rd;
		String line;
		try {
			url = new URL(resource);
			while (tryAgain) {
				result = "";
				try {
					conn = (HttpURLConnection) url.openConnection();
					conn.setConnectTimeout(10000);
					conn.setReadTimeout(10000);
					conn.setRequestProperty("Accept", "application/json");
					conn.setRequestProperty("Content-Type", "application/json");
					conn.setRequestProperty("Authorization", "SSWS " + token);
					conn.setRequestMethod("GET");

					String ret = conn.getResponseMessage();
					int retCode = conn.getResponseCode();

					logger.debug("retCode = " + retCode);

					if (retCode == 200) {
						tryAgain = false;
						rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
						while ((line = rd.readLine()) != null) {
							result += line;
						}
						rd.close();
					} else if (retCode == 404) {
						tryAgain = false;
						rd = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
						while ((line = rd.readLine()) != null) {
							result += line;
						}
						rd.close();
					} else {
						tryAgain = true;
						rd = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
						while ((line = rd.readLine()) != null) {
							result += line;
						}
						rd.close();
						logger.debug(new Date() + " GET " + resource + " RETURNED " + retCode + ":" + ret);
						logger.debug(new Date() + " ERRORSTREAM = " + result);
					}
				} catch (SocketTimeoutException e) {
					tryAgain = true;
					logger.debug(new Date() + " GET " + resource + " " + e.getLocalizedMessage());
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			logger.error(new Date() + " GET " + resource + " " + e.getLocalizedMessage(), e);
		}
		return result;
	}

	public static String post(String resource, String token, String data, String requestMethod) {
		String res = "";
		try {
			URL url = new URL(resource);
			HttpURLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(100000);
			conn.setReadTimeout(100000);
			conn.setDoOutput(true);
			conn.setRequestProperty("Authorization", "SSWS " + token);
			conn.setRequestProperty("Content-Type", "application/json");

			conn.setRequestMethod(requestMethod);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.write(data.getBytes("UTF-8"));
			wr.flush();
			wr.close();
			String line;
			if (conn.getResponseCode() == 200 || conn.getResponseCode() == 201) {
				BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				while ((line = rd.readLine()) != null) {
					res += line;
				}
				rd.close();
			} else if (conn.getResponseCode() == 204) {
			} else {
				BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
				while ((line = rd.readLine()) != null) {
					res += line;
				}
				rd.close();
				logger.debug(new Date() + " POST " + url.toString() + " OF " + data + " RETURNS "
						+ conn.getResponseCode() + ":" + conn.getResponseMessage());
				logger.debug(new Date() + " ERRORSTREAM = " + res);
			}
		} catch (Exception e) {
			logger.error(
					new Date() + " POST " + resource + " OF " + data + " LOCALIZEDMESSAGE " + e.getLocalizedMessage(), e);
		}
		return res;
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {

		loadParams();

		FileReader fileReader = null;
		CSVParser csvFileParser = null;
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader().withIgnoreSurroundingSpaces(true);
		String tmpPassword = null;
		logger.debug("Start Time "+new Date());
		try {

			
			logger.debug("Renaming the file. " +csvFile);
			String subStr1 = csvFile.substring(0, csvFile.indexOf("."));
			String csvFileToConsider = subStr1+"_InProcess.csv";
			
			
			
			
			// if no csv file exists, exit
			File csvFileObj = new File(csvPath, csvFile);
			if (csvFileObj.exists()){
			Path movefrom = FileSystems.getDefault().getPath(csvPath+csvFile);
			Path target = FileSystems.getDefault().getPath(csvPath+csvFileToConsider);
			
			
			Files.move(movefrom, target,StandardCopyOption.REPLACE_EXISTING);
			File inProcessName = new File(csvPath, csvFileToConsider);
			
			if (!inProcessName.exists()) {
				logger.debug("No inprocess csv file found. Exiting. " + inProcessName.getAbsolutePath());
				return;
			}
			
			

			fileReader = new FileReader(inProcessName);
			csvFileParser = new CSVParser(fileReader, csvFileFormat);
			 
			HashMap<String, JSONObject> csvUserMap = new HashMap<String, JSONObject>();
			String ret = "";
			
			// get all locked users in Okta
			
			logger.debug("Getting Lock Group Details");
			ret = get(tenantUrl + "/groups?q=Lock&limit=1", token);
		
			String lockGroupId = "";
			JSONArray lockGrp = new JSONArray(ret);
			
			lockGroupId = lockGrp.getJSONObject(0).getString("id");
	
			logger.debug("Lock Group ID = " + lockGroupId);
			ret = get(tenantUrl + "/groups/" + lockGroupId + "/users", token);

			JSONArray lockedUsers = new JSONArray(ret);

			for (CSVRecord record : csvFileParser) {
				
				int index = -1;

				logger.debug("User To Be Processed:: " + record.get("Email"));

				String oprId = null;
				
				String emplId = null;
				String firstName = null;
				String lastName = null;
				String middleName = null;
				String preferredFirstName = null;
				String email = null;
				String login = null;
				String personalEmail = null;
				String managerEmplId = null;
				String primaryJobStatus = null;
				

				String hourscomEmail = null;

				emplId = record.get("EMPLID");

				// create JSON object
				JSONObject userObj = new JSONObject();

				// new user in csv file, assumes first entry is primary
				if (!csvUserMap.containsKey(emplId)) {

					oprId = record.get("OPRID");
					// add to password to meet O365 password complexity
					// requirements
					if (record.get("Temp Password").length() < 8)
						tmpPassword = "24Hr" + record.get("Temp Password");
					else
						tmpPassword = record.get("Temp Password");
					firstName = record.get("First Name");
					lastName = record.get("Last Name");
					middleName = record.get("Middle Name");
					preferredFirstName = record.get("Preferred First Name");
					email = record.get("Email");
					login = record.get("Email");
					primaryJobStatus = record.get("Primary Job Status");
					/*if (record.get("Personal Email") != null)
						personalEmail = record.get("Personal Email");*/
					
					//BUSN Email
					if (record.get("BUSN Email") != null)
						personalEmail = record.get("BUSN Email");
					
					managerEmplId = record.get("Manager EMPLID");
					hourscomEmail = record.get("24hourfit.com Email");
					

					userObj = new JSONObject();

					csvUserMap.put(emplId, userObj);

					// construct user object
					userObj.put("profile", new JSONObject());
					userObj.getJSONObject("profile").put("firstName", firstName);
					userObj.getJSONObject("profile").put("lastName", lastName);
					userObj.getJSONObject("profile").put("email", email);
					userObj.getJSONObject("profile").put("login", login);
					userObj.getJSONObject("profile").put("middleName", middleName);
					userObj.getJSONObject("profile").put("preferredFirstName", preferredFirstName);
					userObj.getJSONObject("profile").put("managerId", managerEmplId);
					userObj.getJSONObject("profile").put("oprId", oprId);
					userObj.getJSONObject("profile").put("companyEmail", hourscomEmail);
					userObj.getJSONObject("profile").put("primaryJobStatus", primaryJobStatus);
					userObj.getJSONObject("profile").put("secondEmail", personalEmail);
					userObj.getJSONObject("profile").put("status", new JSONArray());
					userObj.getJSONObject("profile").put("location", new JSONArray());
					userObj.getJSONObject("profile").put("jobCode", new JSONArray());
					userObj.getJSONObject("profile").put("primaryJob", new JSONArray());
					userObj.getJSONObject("profile").put("crmBU", new JSONArray());
					
					userObj.getJSONObject("profile").put("employeeNumber", emplId);
					
					
					// construct password object
					logger.debug("Setting Password in user object..." + login);
					
					JSONObject valueObj = new JSONObject();
					valueObj.put("value", tmpPassword);
					JSONObject passwordObj = new JSONObject();
					passwordObj.put("password", valueObj);
					userObj.put("credentials", passwordObj);

					//userObj.getJSONObject("profile").put("credentials", passwordObj);

				}
				// duplicate user in CSV file
				else {
					userObj = csvUserMap.get(emplId);
				}

				// add multi-valued attributes, only if that status is active
				
					if (record.get("STATUS") != null) {
					userObj.getJSONObject("profile").getJSONArray("status").put(record.get("STATUS"));

					if (record.get("DEPTID") != null)
						userObj.getJSONObject("profile").getJSONArray("location").put(record.get("DEPTID"));

					if (record.get("JOBCODE") != null)
						userObj.getJSONObject("profile").getJSONArray("jobCode").put(record.get("JOBCODE"));

					if (record.get("Primary Job") != null)
						userObj.getJSONObject("profile").getJSONArray("primaryJob").put(record.get("Primary Job"));
					
					if (record.get("CRM BU") != null)
						userObj.getJSONObject("profile").getJSONArray("crmBU").put(record.get("CRM BU"));
				}
			}
			logger.debug("CSV Length to be assigned to JSON Obj = "+csvUserMap.size());
			// process all JSON objects
			for (JSONObject userObj : csvUserMap.values()) {
				
				logger.debug("User Object Length = "+userObj.length());

				String login = userObj.getJSONObject("profile").getString("login");
				String val = get(tenantUrl + "/users/" + login, token);

				logger.debug("val = " + val);
				JSONObject oktaUserObj = new JSONObject(val);
				
				
				// user not found
				if (oktaUserObj.has("errorCode") && oktaUserObj.getString("errorCode").equals("E0000007")) {
					
					
					logger.debug("Creating New User..." + login);
					ret = post(tenantUrl + "/users?activate=false", token, userObj.toString(), "POST");
					
					// activate the user
					logger.debug("Activating New User..." + login);
					ret = post(tenantUrl + "/users/" + login + "/lifecycle/activate?sendEmail=false", token, "","POST");
					
					//expire user password
					logger.debug("Expiring New User Password..." + login);
					ret = post(tenantUrl + "/users/" + login + "/lifecycle/expire_password", token, "","POST");
					
					logger.debug("ret = " + ret);
					
			
					
					
					
				}
				// Update Case Start
				else if (!oktaUserObj.has("errorCode")) {
					
					
					userObj.remove("credentials");

					String newPrimaryJobStatus = userObj.getJSONObject("profile").getString("primaryJobStatus");
					String oktaStatus = oktaUserObj.getString("status");
					String oktaId = oktaUserObj.getString("id");

					logger.debug("newPrimaryJobStatus = " + newPrimaryJobStatus);
					logger.debug("oktaStatus = " + oktaStatus);
					boolean isUserLocked = isUserMemberOf(login, lockedUsers);

					// if user is still terminated, no action to take
					if ((oktaStatus.equalsIgnoreCase("DEPROVISIONED") || oktaStatus.equalsIgnoreCase("PASSWORD_EXPIRED") || oktaStatus.equalsIgnoreCase("LOCKED_OUT"))){
						if (newPrimaryJobStatus.equalsIgnoreCase("T") && isUserLocked) {
						logger.debug("user already deprovisioned nothing to do:: Login = " + login);
						continue;
					}
					}
					// user is re-activated
					else if (oktaStatus.equalsIgnoreCase("DEPROVISIONED")
							&& !newPrimaryJobStatus.equalsIgnoreCase("T")) {
						logger.debug("user re-activating login = " + login);
						ret = post(tenantUrl + "/users/" + login + "/lifecycle/activate?sendEmail=false", token, "",
								"POST");
						logger.debug("ret = " + ret);
					}

					
					// lock user
					/*if (!isUserLocked && (newPrimaryJobStatus.equalsIgnoreCase("L") || newPrimaryJobStatus.equalsIgnoreCase("T"))) {
						logger.debug("lock user login = " + login);
						ret = post(tenantUrl + "/groups/" + lockGroupId + "/users/" + oktaId, token, "", "PUT");
						logger.debug("ret = " + ret);
					}*/
					// unlock user
					else if (isUserLocked && (!newPrimaryJobStatus.equalsIgnoreCase("L") || !newPrimaryJobStatus.equalsIgnoreCase("T"))) {
						logger.debug("unlock user login = " + login);
						ret = post(tenantUrl + "/groups/" + lockGroupId + "/users/" + oktaId, token, "", "DELETE");
						logger.debug("ret = " + ret);
					}
					
					if (oktaStatus.equalsIgnoreCase("DEPROVISIONED") && newPrimaryJobStatus.equalsIgnoreCase("A")){
						
						logger.debug("Activating New User..." + login);
						ret = post(tenantUrl + "/users/" + login + "/lifecycle/activate?sendEmail=false", token, "","POST");
						
						
					}

					// check if anything meaningful on the user object has
					// changed
					boolean isUpdated = !usersAreEqual(oktaUserObj, userObj);
					logger.debug("Did User Get Updated = " + isUpdated);
					
					if (isUpdated) {
						
						//Remove entry for Status=T Start
						
						
						
						//Remove entry for Status=T End
						
						// if user has updates, set lastUpdatedDate
						userObj.getJSONObject("profile").put("lastUpdatedDate", new Date());

						logger.debug("updating user login = " + login);
						ret = post(tenantUrl + "/users/" + login, token, userObj.toString(), "POST");
						logger.debug("ret = " + ret);

						// terminate user
						if (!oktaStatus.equalsIgnoreCase("DEPROVISIONED")
								&& newPrimaryJobStatus.equalsIgnoreCase("T")) {
							logger.debug("deactivating user login = " + login);
							ret = post(tenantUrl + "/users/" + login + "/lifecycle/deactivate", token, "", "POST");
							logger.debug("ret = " + ret);
						}
					}
				}
				// unknown error
				else
				{
					logger.error("Error while processing user login = " + login + ". val = " + val);
				}
			}

			logger.debug("Processed all the records:: " + new Date());
			fileReader.close();

			logger.debug("Archiving File " + csvFileToConsider + " To " + archivePath);
			archiveFile(csvPath, csvFileToConsider, archivePath);
			}//If File Exist
		else{
			logger.debug("No csv file found. Exiting. "+csvFile);
			return;
		
		}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		logger.debug("End Time "+new Date());
	}

	public static boolean isUserMemberOf(String login, JSONArray groupMembers) {
		for (int i = 0; i < groupMembers.length(); i++)
			if (groupMembers.getJSONObject(i).getJSONObject("profile").getString("login").equalsIgnoreCase(login))
				return true;
		return false;
	}

	public static boolean usersAreEqual(JSONObject oldObj, JSONObject newObj) {
		
		
		boolean flag = true;
		// compare strings
		List<String> stringAttrs = Arrays.asList("firstName", "lastName", "email", "login", "middleName",
				"preferredFirstName", "managerId", "oprId", "companyEmail", "primaryJobStatus","employeeNumber","secondEmail");

		for (String stringAttr : stringAttrs) {
			if (!oldObj.getJSONObject("profile").getString(stringAttr)
					.equals(newObj.getJSONObject("profile").getString(stringAttr))) {
				logger.debug("change found in stringAttr = " + stringAttr);
				return false;
			}
		}
		logger.debug("usersAreEqual strings match");
		//Order is not the same Start
		
		String sequencedString = null;
		String sequenceFromCsv = null;
		
		ArrayList<String> csvValues = new ArrayList<String>();
		ArrayList<String> oktaValues = new ArrayList<String>();
		
		logger.debug("New Object Length = " + newObj.getJSONObject("profile").getJSONArray("location").length());
for (int i =0 ;i<newObj.getJSONObject("profile").getJSONArray("location").length();i++){
			
			sequenceFromCsv = newObj.getJSONObject("profile").getJSONArray("status").get(i).toString()+"|"+
					newObj.getJSONObject("profile").getJSONArray("location").get(i).toString()+"|"+
					newObj.getJSONObject("profile").getJSONArray("jobCode").get(i).toString()+"|"+
					newObj.getJSONObject("profile").getJSONArray("primaryJob").get(i).toString()+"|"+
					newObj.getJSONObject("profile").getJSONArray("crmBU").get(i).toString();
					
			logger.debug("String from csv = " + sequenceFromCsv);
			
			if (newObj.getJSONObject("profile").getJSONArray("status").get(i).toString().equalsIgnoreCase("T")){
				logger.debug("Removing entry from location and jobcode as status retrieved is = " + newObj.getJSONObject("profile").getJSONArray("status").get(i).toString());
				
				removeEntry(newObj, i);
				
			}
			
			csvValues.add(sequenceFromCsv.trim());
			
			
}
			
		for (int j= 0 ;j<oldObj.getJSONObject("profile").getJSONArray("location").length();j++){
			 sequencedString = oldObj.getJSONObject("profile").getJSONArray("status").get(j).toString()+"|"+
					oldObj.getJSONObject("profile").getJSONArray("location").get(j).toString()+"|"+
					oldObj.getJSONObject("profile").getJSONArray("jobCode").get(j).toString()+"|"+
					oldObj.getJSONObject("profile").getJSONArray("primaryJob").get(j).toString()+"|"+
					oldObj.getJSONObject("profile").getJSONArray("crmBU").get(j).toString();
			
			 logger.debug("String to Compare = " + sequencedString);
			
			 oktaValues.add(sequencedString.trim());
			
			
			
		}
		
		for (String temp2 : csvValues){
			logger.debug("temp2 = "+temp2);
			logger.debug("OktaValues"+oktaValues);
			
			
			if (oktaValues.contains(temp2)){
				continue;
				
			}
			
			else{
				flag = false;
				break;
				
			}
			
		}
		
	
		logger.debug("Returning "+flag);
		return flag;
		
		
		


	}

	public static void loadParams() {
		Properties props = new Properties();
		InputStream is = null;

		// First try loading from the current directory
		try {
			File f = new File("Initialize.properties");
			is = new FileInputStream(f);
		} catch (Exception e) {
			is = null;
		}

		try {
			if (is == null) {

				is = Import24HourFitnessPSCSVUsers.class.getResourceAsStream("Initialize.properties");
			}

			props.load(is);
		} catch (Exception e) {
			e.printStackTrace();
		}

		csvPath = props.getProperty("CSVPath");
		csvFile = props.getProperty("CSVFile");
		archivePath = props.getProperty("ArchivePath");
		tenantUrl = props.getProperty("TenantUrl");
		token = props.getProperty("Token");

		logger.debug("CSV Path:: " + csvPath);
		logger.debug("CSV File:: " + csvFile);
		logger.debug("Tenant URL:: " + tenantUrl);
	}

	public static void archiveFile(String srcPath, String srcFile, String archivalPath){
		
		Path movefrom = FileSystems.getDefault().getPath(srcPath+srcFile);
		
		srcFile = srcFile.replace("_InProcess", "");
		
		String subStr1 = srcFile.substring(0, csvFile.indexOf("."));
		
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd HH_mm_ss");
		String destFile = subStr1+"_" + dateFormat.format(new Date())+".csv";
		Path target = FileSystems.getDefault().getPath(archivalPath+destFile);
		
		try {
			Files.move(movefrom, target,StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
		
			e.printStackTrace();
		}
		
		
	}
	
	public static void removeEntry(JSONObject newObj, int index){
		logger.debug("Removing location, jobcode, primary job and crmBU value as job status retrieved is T");
		
		if (newObj.getJSONObject("profile").getJSONArray("location").get(index) != null && newObj.getJSONObject("profile").getJSONArray("jobCode").get(index) != null && newObj.getJSONObject("profile").getJSONArray("primaryJob").get(index) != null && newObj.getJSONObject("profile").getJSONArray("crmBU").get(index) != null && newObj.getJSONObject("profile").getJSONArray("status").get(index) != null){
			
			newObj.getJSONObject("profile").getJSONArray("location").remove(index);
		
			newObj.getJSONObject("profile").getJSONArray("jobCode").remove(index);
		
			newObj.getJSONObject("profile").getJSONArray("primaryJob").remove(index);
		
			newObj.getJSONObject("profile").getJSONArray("crmBU").remove(index);
			
			newObj.getJSONObject("profile").getJSONArray("status").remove(index);
		
			post(tenantUrl + "/users/" + newObj.getJSONObject("profile").getString("login"), token, newObj.toString(), "POST");
		}
		
	}

}